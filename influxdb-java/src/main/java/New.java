import com.common.utils.influxdb.InfluxDBConnection;
import org.influxdb.dto.QueryResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class New {
    public static Object data_collection() {
        InfluxDBConnection influxDBConnection = new InfluxDBConnection("admin", "admin", "http://localhost:8086", "mydata", "hour");
        QueryResult results = influxDBConnection.query("SELECT * FROM wyy limit 10");
        //results.getResults()是同时查询多条SQL语句的返回值，此处我们只有一条SQL，所以只取第一个结果集即可。
        QueryResult.Result oneResult = results.getResults().get(0);
        List<List<Object>> valueList = oneResult.getSeries().stream().map(QueryResult.Series::getValues).collect(Collectors.toList()).get(0);
        List<String> list = new ArrayList<String>();
        for (List<Object> value : valueList) {
            Map<String, String> map = new HashMap<String, String>();
            // 数据库中字段1取值
            String field1 = value.get(4) == null ? null : value.get(4).toString();
            list.add(field1);  //向列表中添加数据

        }
        return list;


        }
    public static void main(String[] args) {
        System.out.println(data_collection());

    }

}
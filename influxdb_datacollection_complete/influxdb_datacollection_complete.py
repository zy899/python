import jpype
from jpype import *
import os.path
jarpath = os.path.abspath('.')
jpype.startJVM(r"C:\Program Files\Java\jre1.8.0_181\bin\server\jvm.dll",  "-ea",
               "-Djava.class.path=%s" % ('D:\pycharm_project\inluxdb_get_project\influxdb-java.jar'))
JDClass = JClass("New")# 申请一个 Java 类
jd = JDClass()
jprint = java.lang.System.out.println
jprint(jd.data_collection())   #调用该类中的 sayHello 函数，并用 Java 输出函数打印 Java 返回值

# 关闭 Java 虚拟机，可写可不写，不写会在程序结束时自动关闭
jpype.shutdownJVM()

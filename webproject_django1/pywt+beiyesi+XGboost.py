#!/usr/bin/env python
# coding=utf-8
import pandas as pd
import numpy as np
np.set_printoptions(suppress=True)
from sklearn.model_selection import KFold
# Scipy.io进行mat文件的写入与读取
import scipy.io as scio
import sklearn
import matplotlib.pyplot as plt
from sklearn import svm
# pywt用于小波变换的开源库
import pywt
from bayes_opt import BayesianOptimization
from sklearn.model_selection import cross_val_score
from xgboost import XGBClassifier
from matplotlib.font_manager import FontProperties

#读取从电器采集下来的.mat数据
mat1 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/97.mat')# 正常
mat2 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/118.mat')#滚珠
mat3 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/0.3556mm/197.mat')
mat4 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/0.5334mm/234.mat')
mat5 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/105.mat')#内圈
mat6 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/0.3556mm/169.mat')
mat7 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/0.5334mm/209.mat')
mat8 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/130.mat')#外圈
mat9 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/0.3556mm/185.mat')
mat10 = scio.loadmat(u'D:/motorproject/algorithm/mat_data/0.5334mm/222.mat')


b1 = mat1['X097_FE_time']#正常的轴承数据
b2 = mat2['X118_FE_time']#0.1778mm滚珠故障Ball的轴承数据
b3 = mat3['X197_FE_time']#0.3556mm滚珠故障Ball的轴承数据
b4 = mat4['X234_FE_time']#0.5334mm滚珠故障Ball的轴承数据
b5 = mat5['X105_FE_time']#0.1778mm内圈故障Inner Race的轴承数据
b6 = mat6['X169_FE_time']#0.3556mm内圈故障Inner Race的轴承数据
b7 = mat7['X209_FE_time']#0.5334mm内圈故障Inner Race的轴承数据
b8 = mat8['X130_FE_time']#0.1778mm外圈故障Inner Race的轴承数据
b9 = mat9['X185_FE_time']#0.3556mm外圈故障Outer Race的轴承数据
b10 = mat10['X222_FE_time']#0.5334mm外圈故障Outer Race的轴承数据
# 取0到47999个元素一共48000个，的第一列的值生成一个列表
a = b1[0:48000, 0]
b21 = b2[0:16000, 0]
b22 = b3[0:16000, 0]
b23 = b4[0:16000, 0]
b = np.hstack((b21, b22, b23))# 将b21，b22,b23这三个数据水平平铺。
c31 = b5[0:16000, 0]
c32 = b6[0:16000, 0]
c33 = b7[0:16000, 0]
c = np.hstack((c31, c32, c33))
d41 = b8[0:16000, 0]
d42 = b9[0:16000, 0]
d43 = b10[0:16000, 0]
d = np.hstack((d41, d42, d43))
# zip打包两个数据的对应元素，打包后的元素为两个元素的水平组合。
def list_of_groups(init_list, children_list_len):
    list_of_groups = zip(*(iter(init_list),)*children_list_len)
    end_list = [list(i) for i in list_of_groups]
    count = len(init_list) % children_list_len
    # 如果count不等于0,则end_list.append。=0就直接返回end_list。
    end_list.append(init_list[-count:]) if count !=0 else end_list
    return end_list

old_a = list_of_groups(a, 300)
old_b = list_of_groups(b, 300)
old_c = list_of_groups(c, 300)
old_d = list_of_groups(d, 300)

new_a = []
new_b = []
new_c = []
new_d = []

for x in old_a:
    # 创建小波包对象
    wp = pywt.WaveletPacket(x, wavelet='db3', mode='symmetric', maxlevel=3)
    # 访问小波包的子节点，分3层最底层8个节点。
    aaa = wp['aaa'].data  # 第1个节点
    aad = wp['aad'].data  # 第2个节点
    ada = wp['ada'].data  # 第3个节点
    add = wp['add'].data  # 第4个节点
    daa = wp['daa'].data  # 第5个节点
    dad = wp['dad'].data  # 第6个节点
    dda = wp['dda'].data  # 第7个节点
    ddd = wp['ddd'].data  # 第8个节点
    # 求取节点的范数
    ret1 = np.linalg.norm(aaa, ord=None)  # 矩阵元素平方和再开方
    ret2 = np.linalg.norm(aad, ord=None)
    ret3 = np.linalg.norm(ada, ord=None)
    ret4 = np.linalg.norm(add, ord=None)
    ret5 = np.linalg.norm(daa, ord=None)
    ret6 = np.linalg.norm(dad, ord=None)
    ret7 = np.linalg.norm(dda, ord=None)
    ret8 = np.linalg.norm(ddd, ord=None)
    y = [ret1, ret2, ret3, ret4, ret5, ret6, ret7, ret8]
    new_a.append(y)
print(new_a)
print(len(new_a))
for x in old_b:
    wp = pywt.WaveletPacket(x, wavelet='db3', mode='symmetric', maxlevel=3)
    aaa = wp['aaa'].data  # 第1个节点
    aad = wp['aad'].data  # 第2个节点
    ada = wp['ada'].data  # 第3个节点
    add = wp['add'].data  # 第4个节点
    daa = wp['daa'].data  # 第5个节点
    dad = wp['dad'].data  # 第6个节点
    dda = wp['dda'].data  # 第7个节点
    ddd = wp['ddd'].data  # 第8个节点
    # 求取节点的范数
    ret1 = np.linalg.norm(aaa, ord=None)  # 第一个节点系数求得的范数/ 矩阵元素平方和开方
    ret2 = np.linalg.norm(aad, ord=None)
    ret3 = np.linalg.norm(ada, ord=None)
    ret4 = np.linalg.norm(add, ord=None)
    ret5 = np.linalg.norm(daa, ord=None)
    ret6 = np.linalg.norm(dad, ord=None)
    ret7 = np.linalg.norm(dda, ord=None)
    ret8 = np.linalg.norm(ddd, ord=None)
    y = [ret1, ret2, ret3, ret4, ret5, ret6, ret7, ret8]
    new_b.append(y)
for x in old_c:
    wp = pywt.WaveletPacket(x, wavelet='db3', mode='symmetric', maxlevel=3)
    aaa = wp['aaa'].data  # 第1个节点
    aad = wp['aad'].data  # 第2个节点
    ada = wp['ada'].data  # 第3个节点
    add = wp['add'].data  # 第4个节点
    daa = wp['daa'].data  # 第5个节点
    dad = wp['dad'].data  # 第6个节点
    dda = wp['dda'].data  # 第7个节点
    ddd = wp['ddd'].data  # 第8个节点
    # 求取节点的范数
    ret1 = np.linalg.norm(aaa, ord=None)  # 第一个节点系数求得的范数/ 矩阵元素平方和开方
    ret2 = np.linalg.norm(aad, ord=None)
    ret3 = np.linalg.norm(ada, ord=None)
    ret4 = np.linalg.norm(add, ord=None)
    ret5 = np.linalg.norm(daa, ord=None)
    ret6 = np.linalg.norm(dad, ord=None)
    ret7 = np.linalg.norm(dda, ord=None)
    ret8 = np.linalg.norm(ddd, ord=None)
    y = [ret1, ret2, ret3, ret4, ret5, ret6, ret7, ret8]
    new_c.append(y)
for x in old_d:
    wp = pywt.WaveletPacket(x, wavelet='db3', mode='symmetric', maxlevel=3)
    aaa = wp['aaa'].data  # 第1个节点
    aad = wp['aad'].data  # 第2个节点
    ada = wp['ada'].data  # 第3个节点
    add = wp['add'].data  # 第4个节点
    daa = wp['daa'].data  # 第5个节点
    dad = wp['dad'].data  # 第6个节点
    dda = wp['dda'].data  # 第7个节点
    ddd = wp['ddd'].data  # 第8个节点
    # 求取节点的范数
    ret1 = np.linalg.norm(aaa, ord=None)  # 第一个节点系数求得的范数/ 矩阵元素平方和开方
    ret2 = np.linalg.norm(aad, ord=None)
    ret3 = np.linalg.norm(ada, ord=None)
    ret4 = np.linalg.norm(add, ord=None)
    ret5 = np.linalg.norm(daa, ord=None)
    ret6 = np.linalg.norm(dad, ord=None)
    ret7 = np.linalg.norm(dda, ord=None)
    ret8 = np.linalg.norm(ddd, ord=None)
    y = [ret1, ret2, ret3, ret4, ret5, ret6, ret7, ret8]
    new_d.append(y)
# 行合并
x = np.row_stack((new_a, new_b, new_c, new_d))
# 取这个另x本身-x每一列的平均值再赋给x（每一列有8个元素即这8个数的平均值）。
x -= np.mean(x, axis=0)
# 再除以列标准差 进行归一化处理
x /= np.std(x, axis=0)
# 打印矩阵x的维度
print(x.shape)
# 制造一个由160个0、1、2、3组成的一行640列的元素，给X打标记
y = [[0]*160, [1]*160, [2]*160, [3]*160]
# 将y转化成4行160列的矩阵
y = np.array(y)
# 将y转换成1行，其他行全部并到第一行里（将一个数据堆叠到第一行）
y = y.reshape(-1, 1)
# 将x，y每列按元素堆叠到一起，此时xy为维度9长度640的列表
xy = np.hstack((x, y))
# 打乱数据集（按元素打乱，每个元素内9个数据不会打乱）
np.random.shuffle(xy)
print(xy.shape)
# 取出整个列表每个元素的前八个数据（特征值）
x = xy[0:640, :-1]
print(x.shape)
# 取出最后一个数据（标记）
y = xy[0:640, -1:]
print(y.shape)
# 创建一个2维单元格
xy = pd.DataFrame(list(xy))
# 对xy进行交叉验证，评价和预测该模型，尤其是训练好的模型在新数据上的表现，
# 可以在一定程度上减小过拟合，以从有限的数据中获取尽可能多的有效信息
kf = KFold(n_splits=10, shuffle=True)#10折交叉验证
# 每次用其中一个子集当作验证集，剩下的n_splits-1个作为训练集，进行n_splits次训练和测试，得到n_splits个结果
for train_index, test_index in kf.split(xy):
    x_train, x_test = x[train_index], x[test_index]
    y_train, y_test = y[train_index], y[test_index]
    # %s占位符，用来代替后面“%”后的内容
    print('x_train:%s , x_test: %s ' %(x_train, x_test))
    print('y_train:%s , y_test: %s ' % (y_train, y_test))
training_data = zip(x_train, y_train)
test_data = zip(x_test, y_test)
print(type(training_data))
print(len(list(training_data)))
print(len(y_test))
# 建立xgboost模型，能在每一轮迭代中使用交叉验证
clf = XGBClassifier( use_label_encoder=False)
# 计算XGB训练（参数默认）模型的得分的平均值（得分标准为准确性）
print(np.mean(cross_val_score(clf, x_train, y_train.ravel(), cv=10, scoring='accuracy'))) # cv=10交叉验证的折数/迭代次数
def rf_cv(n_estimators, min_child_weight, gamma, max_depth):
    # val为调好参数的xgboost模型的评价分数平均值。
    val = cross_val_score(
        XGBClassifier(n_estimators=int(n_estimators),# 总共迭代的次数
            min_child_weight=int(min_child_weight),# 最小样本权重的和，值越大，越容易欠拟合；值越小，越容易过拟合
                      # 过拟合：机器学习了全局特征时把局部特征学进去，导致以局部特征为标准来区分事物的类别导致不准确性
            gamma=min(gamma, 0.999), # float，指定节点分裂所需的最小损失函数下降值，
                      # 在节点分裂时，只有分裂后损失函数的值下降了，才会分裂这个节点。
            max_depth=int(max_depth),# 树的最大深度，防止过拟合
            learning_rate=0.1, # 控制每次迭代更新权重时的步长，值越小训练越慢
            use_label_encoder=False

        ),
        x_train, y_train.ravel(), scoring='accuracy', cv=10
    ).mean()
    return val

# 利用贝叶斯优化算法将参数进行优化达到平衡的状态
rf_bo = BayesianOptimization(
        rf_cv,
        {'n_estimators': (10, 250),
        'min_child_weight': (2, 25),
        'gamma': (0.1, 0.999),
        'max_depth': (3, 15)}
    )
# 开始优化
rf_bo.maximize()
